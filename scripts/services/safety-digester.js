// used to ensure we don't trigger concurrent angular digests

angular.module('shipitVotingApp')
       .service('safetyDigester', ['$rootScope', function ($rootScope) {

  var queued = false;
  var digest = function() {
    var phase = $rootScope.$$phase;
    if (phase === '$apply' || phase === '$digest') {
      if (!queued) {
        setTimeout(function() {
          digest($rootScope);
          queued = false;
        }, 500);
        queued = true;
      }
    } else {
      $rootScope.$digest();
    }
  };

  return {
    digest: digest
  };

}]);