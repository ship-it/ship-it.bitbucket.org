angular.module('shipitVotingApp')
       .service('shipitFirebase', ['$rootScope', '$location', function ($rootScope, $location) {

      var firebase;
      var authenticated = false;
      var baseUrl = "https://atlassian.firebaseio.com/atlascamp2013/shipit/";

      var child = function(childPath) {
        if (!firebase) {
          firebase = new Firebase(baseUrl);
        }

        if (!authenticated) {
          var suToken = $rootScope.user && $rootScope.user.su;
          if (suToken) {
            firebase.auth(suToken, function(err) {
              if (err) {
                // TODO redirect to error page
                $rootScope.$apply(function(){$location.path('/')});
              } else {
                authenticated = true;
              }
            });            
          }
        }

        return firebase.child(childPath);
      };

      return {
        child: child,
        baseUrl: baseUrl
      };
    }
]);