angular.module('shipitVotingApp')
       .service('idAuthenticator', ['$rootScope', '$routeParams', '$location', 'shipitFirebase', 
        function ($rootScope, $routeParams, $location, shipitFirebase) {
      var usersOnly = function(operation, suOnly) {
        var votersRef = shipitFirebase.child('voter/registered');
        var voterRef = votersRef.child($routeParams.id);

        voterRef.once('value', function(voterSnapshot) {        
          var voter = voterSnapshot.val();
          if (voter === null || (suOnly && !voter.su)) {
            $rootScope.$apply(function(){$location.path('/')});
          } else {
            $rootScope.user = {
              id: $routeParams.id,
              su: voter.su
            };
            operation(voterSnapshot);
          }
        });
      };

      return {
        usersOnly: function(operation) {
          usersOnly(operation, false);
        },
        superUsersOnly: function(operation) {
          usersOnly(operation, true);
        }
      };
    }
]);