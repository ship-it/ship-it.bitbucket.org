'use strict';

angular.module('shipitVotingApp')
  .controller('MainCtrl', ['$scope', 'angularFire', 'shipitFirebase',
    function ($scope, angularFire, shipitFirebase) {
      var url = shipitFirebase.baseUrl + '/voter/requested';
      angularFire(url, $scope, 'requested', []).then(function() {
        $scope.register = function() {
          $scope.submitted = $scope.registration.email;
          $scope.requested.push({email: $scope.registration.email});
        };
      });
    }]
  );
