'use strict';

angular.module('shipitVotingApp')
  .controller('VotersCtrl', ['$scope', 'safetyDigester', 'idAuthenticator', 'shipitFirebase',
    function ($scope, safetyDigester, idAuthenticator, shipitFirebase) {
      idAuthenticator.superUsersOnly(function() {
        $scope.votes = 0;
        $scope.hashes = [];
        var votersRef = shipitFirebase.child("voter/registered");
        votersRef.on("child_added", function(voterSnapshot) {
          $scope.hashes.push(CryptoJS.MD5(voterSnapshot.val().email.toLowerCase()).toString(CryptoJS.enc.Hex));
          safetyDigester.digest();
        });
        var leaderboardRef = shipitFirebase.child("leaderboard");
        leaderboardRef.on("value", function(leaderboardSnapshot) {
          var total = 0;
          leaderboardSnapshot.forEach(function(entry) {
            total += entry.val();
          });
          $scope.votes = total;
          safetyDigester.digest();
        });
      });
    }]
  );
