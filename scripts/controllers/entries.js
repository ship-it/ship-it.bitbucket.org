'use strict';

angular.module('shipitVotingApp')
  .controller('EntriesCtrl', ['$scope', 'safetyDigester', 'idAuthenticator', 'shipitFirebase',
    function ($scope, safetyDigester, idAuthenticator, shipitFirebase) {
      idAuthenticator.superUsersOnly(function(voterSnapshot) {
        var entriesRef = shipitFirebase.child('entry');
        
        entriesRef.on("value", function(entriesSnapshot) {
          $scope.entries = _.map(entriesSnapshot.val(), function (entry, id) {
            // affix id to each entry
            return _.extend(entry, {id: id});
          });
          safetyDigester.digest();
        });

        $scope.addEntry = function() {
          var randomId = Math.floor(Math.random() * 0xffffffffff).toString(16);
          entriesRef.child(randomId).set(_.pick($scope.entry, 'name', 'presenter'));
          $scope.entry = null;
        };

        $scope.removeEntry = function(entry) {
          entriesRef.child(entry.id).remove();        
        };      
      });
    }
  ]);
