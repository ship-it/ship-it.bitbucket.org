'use strict';

angular.module('shipitVotingApp')
  .controller('BallotCtrl', ['$scope', 'safetyDigester', 'idAuthenticator', 'shipitFirebase',
    function ($scope, safetyDigester, idAuthenticator, shipitFirebase) {
      idAuthenticator.usersOnly(function(voterSnapshot) {
        var voterRef = voterSnapshot.ref();
        var entriesRef = shipitFirebase.child("entry");
        entriesRef.on("value", function (entriesSnapshot) {
          $scope.entries = _.map(entriesSnapshot.val(), function (entry, id) {
            // affix id to each entry
            return _.extend(entry, {id: id});
          });
          safetyDigester.digest();
        });
        
        var votesRef = voterRef.child("vote"); 

        $scope.castVote = function(entry) {
          // use timestamp as key so list is FIFO if server-side has to truncate
          votesRef.child(new Date().getTime()).set(entry.id);
        };

        $scope.removeVote = function(entry) {
          votesRef.once("value", function(votesSnapshot) {
            // remove votes for this entry
            votesSnapshot.forEach(function (child) {
              if (child.val() === entry.id) {
                child.ref().remove();
              }
            });
          });
        };

        votesRef.on("value", function(votesSnapshot) {              
          $scope.voted = {};
          votesSnapshot.forEach(function(voteSnapshot) {              
            $scope.voted[voteSnapshot.val()] = true;              
          });
          safetyDigester.digest();
        });
      });
    }
]);
