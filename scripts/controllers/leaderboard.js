'use strict';

angular.module('shipitVotingApp')
  .controller('LeaderboardCtrl', ['$scope', 'safetyDigester', function ($scope, safetyDigester) {
    var shipitRef = new Firebase("https://atlassian.firebaseio.com/atlascamp2013/shipit");

    var entriesRef = shipitRef.child("entry");
    var leaderboardRef = shipitRef.child("leaderboard");

    leaderboardRef.on("value", function(leaderboardSnapshot) {
      var entries = {};
      // create skeleton entries with ids and votes
      leaderboardSnapshot.forEach(function(entryVotesSnapshot) {
        entries[entryVotesSnapshot.name()] = {
          votes: entryVotesSnapshot.val()
        };
      });
      
      entriesRef.once("value", function(entriesSnapshot) {
        // inflate entries with entry data
        entriesSnapshot.forEach(function(entrySnapshot) {
          var entry = entrySnapshot.val();
          var id = entrySnapshot.name();
          _.merge(entries[id], entry, {id: id});
        });
        // strip any cruft votes that don't correspond to real entries
        entries = _.filter(entries, function(entry) {
          return entry.id;
        });
        // sort by vote count
        entries = _.sortBy(entries, function(entry) {
          return -entry.votes;
        });
        $scope.entries = entries;
        safetyDigester.digest();
      });          
    });
}]);
