'use strict';

angular.module('shipitVotingApp', ['firebase'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/leaderboard', {
        templateUrl: 'views/leaderboard.html',
        controller: 'LeaderboardCtrl'
      })
      .when('/secure/:id/entries', {
        templateUrl: 'views/entries.html',
        controller: 'EntriesCtrl'
      })
      .when('/secure/:id/voters', {
        templateUrl: 'views/voters.html',
        controller: 'VotersCtrl'
      })
      .when('/secure/:id/ballot', {
        templateUrl: 'views/ballot.html',
        controller: 'BallotCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
